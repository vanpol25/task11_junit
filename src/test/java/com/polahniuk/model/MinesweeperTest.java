package com.polahniuk.model;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.RepeatedTest;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.mockito.Mockito.*;
import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(MockitoExtension.class)
class MinesweeperTest {

    @InjectMocks
    private static Minesweeper mwm;
    private static Minesweeper mw;
    @Mock
    private static Expression expression;
    private static final int M = 10;
    private static final int N = 10;
    private static final int P = 15;

    @BeforeAll
    static void before() {
        mw = new Minesweeper();
    }

    @BeforeEach
    void setUp() {
        mw.createGame(M, N, P);
    }

    @Test
    void testUserInput() {
        assertThrows(ArrayIndexOutOfBoundsException.class, () -> mw.userInput(15, 15));
    }

    @Test
    void testUserInputExpression() {
        String[] ret = {"1", "1"};
        when(expression.getCoordinates("1:1")).thenReturn(ret);
        assertEquals(mwm.userInputExpression("1:1"), ret);
    }

    @Test
    void testUserInputExpression2() {
        Expression expression2 = mock(Expression.class);
        Minesweeper minesweeper = new Minesweeper();
        minesweeper.createGame(10, 10, 15);
        String[] ret = {"1", "1"};
        when(expression2.getCoordinates("1:1")).thenReturn(ret);
        assertEquals(minesweeper.userInputExpression("1:1"), ret);
        verify(expression2).getCoordinates("1:1");
    }

    @Test
    void testGetBackEndOfBattleground() {
        assertTrue(mw.getBackEndOfBattleground().length == 8 &&
                mw.getBackEndOfBattleground()[0].length == 10);
    }

    @RepeatedTest(10)
    void testGetElementsForWin() {
        int actual = 80;
        int expected = mw.getElementsForWin();
        assertTrue(expected > actual, "act=" + actual + ", exp=" + expected);
    }

    @Test
    void testCreateGame() {
        assertThrows(IllegalArgumentException.class, () -> mw.createGame(5, 6, 9));
    }

    @Test
    void testGetUserArray() {
        assertTrue(mw.getUserArray().length == 8 &&
                mw.getUserArray()[0].length == 10);
    }
}