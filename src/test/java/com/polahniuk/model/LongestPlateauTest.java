package com.polahniuk.model;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class LongestPlateauTest {

    private static LongestPlateau lp;

    @BeforeAll
    static void before() {
        int[] arr = {1, 2, 2, 2, 4, 4, 4, 4, 4, 4, 4, 4, 5, 5, 6, 6, 6, 1, 1, 1, 1, 2, 2, 2, 2, 2, 2, 2, 1, 4, 4, 4, 5, 5};
        lp = new LongestPlateau(arr);
    }

    @Test
    void longestPlateau() {
        LongestPlateau.Plateau plateau = lp.longestPlateau();
        int size = plateau.getSize();
        assertTrue(size == 8);
    }

}