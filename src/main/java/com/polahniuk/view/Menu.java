package com.polahniuk.view;

import com.polahniuk.model.LongestPlateau;
import com.polahniuk.model.Minesweeper;
import com.polahniuk.model.Printable;
import org.apache.logging.log4j.*;

import java.util.*;

/**
 * Main view of project. Show menu in console.
 */
public class Menu {

    private Logger log = LogManager.getLogger(Menu.class);
    private Scanner sc = new Scanner(System.in);
    private Map<String, String> menu;
    private Map<String, Printable> methods;
    private Minesweeper mw;
    private LongestPlateau lp;

    /**
     * Creates methods in constructor due to interface{@link Printable}.
     */
    public Menu() {
        setMenu();
        methods = new LinkedHashMap<>();
        methods.put("1", this::minesweeper);
        methods.put("2", this::longestPlateau);
        show();
    }

    /**
     * UI in console for {@link Minesweeper} game.
     */
    private void minesweeper() {
        mw = new Minesweeper();

        System.out.println("Enter size m:");
        int sizeM = sc.nextInt();

        System.out.println("Enter size n:");
        int sizeN = sc.nextInt();

        System.out.println("Enter percentage of bombs:");
        int p = sc.nextInt();

        mw.createGame(sizeM, sizeN, p);
        int gameCode = mw.getElementsForWin();
        int n;
        int m;
        while (gameCode > 1) {
            soutGame(mw.getUserArray(), sizeM, sizeN);
            System.out.println("Open m:");
            m = sc.nextInt();
            System.out.println("Open n:");
            n = sc.nextInt();

            gameCode = mw.userInput(m - 1, n - 1);
            System.out.println("Elements: " + mw.getElementsForWin());
        }
        if (gameCode == -1) {
            System.out.println("You lose!");
            soutGame(mw.getBackEndOfBattleground(), sizeM, sizeN);
        } else {
            System.out.println("You win!");
            soutGame(mw.getBackEndOfBattleground(), sizeM, sizeN);
        }
    }

    /**
     * Finds the longest plateau of integer array.
     */
    private void longestPlateau() {
        int[] arr = {1, 2, 2, 2, 4, 4, 4, 4, 4, 4, 4, 4, 5, 5, 6, 6, 6, 1, 1, 1, 1, 2, 2, 2, 2, 2, 2, 2, 1, 4, 4, 4, 5, 5};
        lp = new LongestPlateau(arr);
        System.out.println(lp.longestPlateau());
    }

    /**
     * Sout in console {@link Minesweeper#usersBattleground} in comfortable form.
     *
     * @param userArray - array to show.
     * @param sizeM     - size battleground by vertical.
     * @param sizeN     - size battleground by horizontal.
     */
    private void soutGame(char[][] userArray, int sizeM, int sizeN) {
        System.out.print("   ");
        for (int i = 0; i < sizeN; i++) {
            System.out.print((i + 1) + " ");
        }
        System.out.println("N");
        System.out.print("   ");
        for (int i = 0; i < sizeN; i++) {
            System.out.print("_ ");
        }
        System.out.println();

        for (int i = 0; i < sizeM; i++) {
            System.out.print((i + 1) + "| ");
            for (int j = 0; j < sizeN; j++) {
                System.out.print(userArray[i][j] + " ");
            }
            System.out.println();
        }
        System.out.println("M");
    }

    /**
     * Creating menu.
     */
    private void setMenu() {
        menu = new LinkedHashMap<>();
        menu.put("1", "1 - Minesweeper");
        menu.put("2", "2 - Longest plateau");
        menu.put("Q", "Q - Exit");
    }

    /**
     * Show menu in console.
     */
    private void getMenu() {
        System.out.println("\nMENU:");
        for (String key : menu.keySet()) {
            System.out.println(menu.get(key));
        }
    }

    /**
     * Main method to work with user in console.
     */
    private void show() {
        String keyMenu;
        do {
            getMenu();
            System.out.println("Please, select menu point.");
            keyMenu = sc.nextLine().toUpperCase();
            try {
                methods.get(keyMenu).print();
            } catch (Exception e) {
                log.info(e);
            }
        } while (!keyMenu.equals("Q"));
    }

}
