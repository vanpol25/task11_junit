package com.polahniuk.model;

/**
 * Interface help with user input.
 * Uses RegEx.
 */
public interface Expression {
    String[] getCoordinates(String expression);
}
