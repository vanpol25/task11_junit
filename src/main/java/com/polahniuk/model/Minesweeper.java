package com.polahniuk.model;

import java.util.Random;

/**
 * Class create battleground of minesweeper game.
 */
public class Minesweeper {

    /**
     * 2D array of booleans, where true is a bomb.
     */
    private boolean[][] bombArray;
    /**
     * 2D array of chars, where '*' is a bomb.
     * [1-8] is a element near bombs.
     * And empty elements like ' '.
     */
    private char[][] backEndOfBattleground;
    /**
     * Main battleground for user.
     * Shows elements sequence of user input {@link Minesweeper#userInput(int, int)}.
     */
    private char[][] usersBattleground;
    /**
     * Counter for not bomb elements.
     */
    private int elementsForWin;
    /**
     * Size battleground by vertical.
     */
    private int m;
    /**
     * Size battleground by horizontal.
     */
    private int n;
    /**
     * Percentage of bombs.
     */
    private int p;
    private Random random = new Random();
    /**
     * Class help with user input with RegEx in method {@link Minesweeper#userInputExpression(String)}.
     */
    private Expression expression;

    /**
     * Main method to create game.
     *
     * @param m - size battleground by vertical.
     * @param n - size battleground by horizontal.
     * @param p - percentage of bombs.
     * @throws IllegalArgumentException - if e.g. {@link Minesweeper#checkM(int)} injects wrong value.
     */
    public void createGame(int m, int n, int p) throws IllegalArgumentException {
        checkM(m);
        checkN(n);
        checkP(p);
        setBombArray();
        setBackEndOfBattleground();
        setUsersBattleground();
    }

    /**
     * @return - 2D arrays for user. E.g. after users input.
     */
    public char[][] getUserArray() {
        return usersBattleground;
    }

    /**
     * @param m - coordinates by vertical.
     * @param n - coordinates by horizontal.
     * @return - code 0 if win, -1 if lose, > 0 if game in progress.
     * @throws ArrayIndexOutOfBoundsException - users mistake with coordinates.
     */
    public int userInput(int m, int n) throws ArrayIndexOutOfBoundsException {
        if (bombArray[m][n]) {
            elementsForWin = -1;
        } else {
            if (backEndOfBattleground[m][n] == ' ') {
                openSector(m, n);
            } else {
                usersBattleground[m][n] = backEndOfBattleground[m][n];
                elementsForWin--;
            }
        }
        return elementsForWin;
    }

    /**
     * Test class with more convenient users input based on RegEx.
     *
     * @param expression - any expression with two numbers divided by any symbol.
     * @return - massive of numbers like String array.
     */
    public String[] userInputExpression(String expression) {
        String[] coordinates = this.expression.getCoordinates(expression);
        return coordinates;
    }

    /**
     * @return - {@link Minesweeper#backEndOfBattleground} after lose or win game.
     */
    public char[][] getBackEndOfBattleground() {
        return backEndOfBattleground;
    }

    /**
     * When {@link Minesweeper#backEndOfBattleground} element is empty like ' ',
     * we open all nearest elements for {@link Minesweeper#usersBattleground}.
     *
     * @param m - coordinates for element in vertical.
     * @param n - coordinates for element in horizontal.
     */
    private void openSector(int m, int n) {
        int upper = n == 0 ? n : (n - 1);
        int lower = n == this.n - 1 ? n : (n + 1);
        int left = m == 0 ? m : (m - 1);
        int right = m == this.m - 1 ? m : (m + 1);

        for (int i = left; i <= right; i++) {
            for (int j = upper; j <= lower; j++) {
                if (usersBattleground[i][j] == 'O') {
                    elementsForWin--;
                }
                usersBattleground[i][j] = backEndOfBattleground[i][j];
            }
        }

    }

    /**
     * @return - counter {@link Minesweeper#elementsForWin}.
     */
    public int getElementsForWin() {
        return elementsForWin;
    }

    /**
     * Randomly sets bombs in {@link Minesweeper#bombArray}.
     */
    private void setBombArray() {
        bombArray = new boolean[m][n];
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                bombArray[i][j] = !(random.nextInt(100) + 1 >= p);
            }
        }
    }

    /**
     * After {@link Minesweeper#bombArray} is formed, here creates {@link Minesweeper#backEndOfBattleground}.
     * Alse in this method creates {@link Minesweeper#elementsForWin}.
     */
    private void setBackEndOfBattleground() {
        backEndOfBattleground = new char[m][n];
        int bombCount = 0;
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                if (bombArray[i][j]) {
                    backEndOfBattleground[i][j] = '*';
                    bombCount++;
                }
            }
        }
        elementsForWin = m * n - bombCount;

        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                backEndOfBattleground[i][j] = getBackEndOfElement(i, j);
            }
        }
    }

    /**
     * Create {@link Minesweeper#usersBattleground} as a 2D array of symbol 'O'.
     */
    private void setUsersBattleground() {
        usersBattleground = new char[m][n];
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                usersBattleground[i][j] = 'O';
            }
        }
    }

    /**
     * @param m - coordinates for element in vertical.
     * @param n - coordinates for element in horizontal.
     * @return - element for {@link Minesweeper#usersBattleground} in method {@link Minesweeper#userInput(int, int)}.
     */
    private char getBackEndOfElement(int m, int n) {
        if (bombArray[m][n]) {
            return '*';
        }

        int upper = n == 0 ? n : (n - 1);
        int lower = n == this.n - 1 ? n : (n + 1);
        int left = m == 0 ? m : (m - 1);
        int right = m == this.m - 1 ? m : (m + 1);

        final int RADIX = 10;
        int bombCountNearElement = 0;
        for (int i = left; i <= right; i++) {
            for (int j = upper; j <= lower; j++) {
                if (bombArray[i][j]) {
                    bombCountNearElement++;
                }
            }
        }
        return bombCountNearElement == 0 ? ' ' : Character.forDigit(bombCountNearElement, RADIX);
    }

    /**
     * @param m - check for min-max value for {@link Minesweeper#m}.
     * @throws IllegalArgumentException - if value goes beyond.
     */
    private void checkM(int m) throws IllegalArgumentException {
        if (m > 20 || m < 2) {
            throw new IllegalArgumentException();
        }
        this.m = m;
    }

    /**
     * @param n - check for min-max value for {@link Minesweeper#n}.
     * @throws IllegalArgumentException - if value goes beyond.
     */
    private void checkN(int n) throws IllegalArgumentException {
        if (n > 20 || n < 2) {
            throw new IllegalArgumentException();
        }
        this.n = n;
    }

    /**
     * @param p - check for min-max value for {@link Minesweeper#p}.
     * @throws IllegalArgumentException - if value goes beyond.
     */
    private void checkP(int p) throws IllegalArgumentException {
        if (p > 50 || p < 10) {
            throw new IllegalArgumentException();
        }
        this.p = p;
    }
}
