package com.polahniuk.model;

public interface Printable {
    void print() throws Exception;
}
