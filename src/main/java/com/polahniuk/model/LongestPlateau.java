package com.polahniuk.model;

import java.util.ArrayList;
import java.util.List;

/**
 * Given an array of integers, find the length and location of the longest contiguous
 * sequence of equal values where the values of the elements just before and just after this sequence are
 * smaller.
 */
public class LongestPlateau {

    /**
     * List of {@link LongestPlateau.Plateau}.
     * Created in method {@link LongestPlateau#createPlateaus()}.
     */
    private List<Plateau> plateaus;
    /**
     * Main array of elements.
     */
    private int[] array;

    /**
     * Constructor with array in parameter. Has method inside {@link LongestPlateau#createPlateaus()}.
     *
     * @param array - array of integer elements.
     */
    public LongestPlateau(int[] array) {
        plateaus = new ArrayList<>();
        this.array = array;
        createPlateaus();
        if (array.length > 0) {
            createPlateaus();
        }
    }

    /**
     * Method that create {@link LongestPlateau#plateaus}.
     */
    private void createPlateaus() {
        int start = 0;
        int end = 0;
        int present = array[0];

        int i = 1;
        while (i < array.length) {
            if (array[i] != array[i - 1]) {
                end = i;
                plateaus.add(new Plateau(start, end, present));
                start = i;
                present = array[i];
            }
            i++;
        }
    }

    /**
     * Find the longest contiguous sequence of equal values.
     *
     * @return - the longest contiguous sequence of equal values in object {@link LongestPlateau.Plateau}.
     */
    public Plateau longestPlateau() {
        return plateaus.stream().max((a, b) -> a.size > b.size ? 1 : -1).orElse(null);
    }

    /**
     * Inner class which create main information of sequence of elements.
     */
    public class Plateau {
        /**
         * Value of element.
         */
        private int element;
        /**
         * Start index in {@link LongestPlateau#array}.
         */
        private int startPlateau;
        /**
         * End index in {@link LongestPlateau#array}.
         */
        private int endPlateau;
        /**
         * Size of sequence.
         */
        private int size;

        /**
         * @param startPlateau - {@link Plateau#startPlateau}.
         * @param endPlateau   - {@link Plateau#endPlateau}.
         * @param element      -  - {@link Plateau#element}.
         */
        private Plateau(int startPlateau, int endPlateau, int element) {
            this.startPlateau = startPlateau;
            this.endPlateau = endPlateau;
            this.element = element;
            this.size = endPlateau - startPlateau;
        }

        /**
         * @return - {@link Plateau#size}.
         */
        public int getSize() {
            return size;
        }

        /**
         * @return - toString of {@link Plateau}.
         */
        @Override
        public String toString() {
            return "Plateau{" +
                    "element=" + element +
                    ", startPlateau=" + startPlateau +
                    ", endPlateau=" + endPlateau +
                    ", size=" + size +
                    '}';
        }
    }

}
